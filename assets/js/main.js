$(document).ready(function() {
    $('#slider .owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        items: 4,
        autoplay:false,
        autoplayTimeout:1000,
        autoplayHoverPause:true,
        dots:false,
        navText: ["<img src='assets/images/prev.png' class = 'img-offer1'>","<img src='assets/images/next.png' class = 'img-offer2'>"],
        responsiveClass:true,
        responsive:{
        0:{
            items:1,
            nav:false
        },
        767:{
            items:2,
            nav:false
        },
        1024:{
            items:1,
            nav:true,
            loop:true
        }
    }
    })
});

(function($) {
    var $h1 = $(document).height();
    var $show_form = false;
    $(window).scroll(function() {
        if (($(window).scrollTop() > $h1 * 0.7) && (!$show_form)) {
            if ($(window).width() <= 992)
                $('.auto-popup').show();
            $('#form_popup').addClass('active');
            $show_form = true;
        }
    });
    setTimeout(function() {
        if (!$show_form) {
            if ($(window).width() <= 992)
                $('.auto-popup').show();
            $('#form_popup').addClass('active');
            $show_form = true;
        }
    }, 25000);
    $('#form_popup .close').click(function(e) {
        $('#form_popup').removeClass('active');
        if ($(window).width() <= 992)
            $('.auto-popup').hide();
    });
    $('#form_popup .wpcf7').on('wpcf7mailsent', function(event) {
        setTimeout(function() {
            $('#form_popup').removeClass('active');
            if ($(window).width() <= 992)
                $('.auto-popup').hide();
        }, 10000);
    });
    if ($(window).width() <= 992) {
        $('#form_popup .close').click(function(e) {
            $('.auto-popup').hide();
            $('#form_popup').removeClass('active');
        });
    }
}
)(jQuery);
